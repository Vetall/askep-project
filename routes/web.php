<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main Page
Route::get('/', 'MainController@index')->name('main-page');

// Page statistic doctors
Route::get('/statistic-doctors', 'MainController@statisticDoctor')->name('statistic-doctors');

// Page statistic diagnoses
Route::get('/statistic-diagnoses', 'MainController@statisticDiagnose')->name('statistic-diagnoses');
