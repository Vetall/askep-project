<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Askep.net</title>
    <link rel="stylesheet" href="{{ url('css/main.css')}}" />
    <link rel="stylesheet" href="{{ url('css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{ url('css/adminlte.css')}}">
</head>