@extends('layouts.master')

@section('content')

    <div class="wrapper">

        <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Diagnose Tables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('main-page')}}">Home</a></li>
                            <li class="breadcrumb-item active">Table of diagnose</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

            <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Порахувати к-сть поставлених діагнозів</h3>
                        </div>

                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Область</th>
                                    <th>Назва спеціальності лікаря</th>
                                    <th>Кількість діагнозів на місць</th>
                                    {{--<th>Бронхіт</th>--}}
                                    {{--<th>Гастродуоденіт</th>--}}
                                    {{--<th>Ішемія</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($diagnoses as $diagnose)
                                <tr>
                                    <td>{{$diagnose['state']}}</td>
                                    <td>{{$diagnose['title']}}</td>
                                    <td>{{$diagnose['count(form_data.id)']}}</td>
{{--                                    <td>{{$diagnose['diagnos']}}</td>--}}
                                    {{--<td>{{$diagnose['diagnos']}}</td>--}}
                                    {{--<td>{{$diagnose['diagnos']}}</td>--}}
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Область</th>
                                    <th>Назва спеціальності лікаря</th>
                                    <th>Кількість діагнозів на місць</th>
                                    {{--<th>Бронхіт</th>--}}
                                    {{--<th>Гастродуоденіт</th>--}}
                                    {{--<th>Ішемія</th>--}}
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-sm-7">
                        <div class="dataTables_paginate paging_simple_numbers text-right" id="example1_paginate">
                            <ul class="pagination">
                                12
                            </ul>
                        </div>
                    </div>
                </div>
                </section>
    </div>
</div>
@endsection