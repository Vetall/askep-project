@extends('layouts.master')

@section('content')

<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            STATISTICS ASKEP.NET
        </div>

        <div class="links">
            <a href="{{route('statistic-doctors')}}">Number of doctors</a>
            <a href="{{route('statistic-diagnoses')}}">Number of diagnoses</a>

        </div>
    </div>
</div>

@endsection