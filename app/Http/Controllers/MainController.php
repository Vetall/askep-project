<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('pages.index');
    }

    /**
     *
     * Statistic Doctors
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statisticDoctor()
    {
//        SELECT address . state, specialties . title, COUNT(specialties . id) as count_specialist FROM address INNER JOIN hospitals ON hospitals . address_id = address . id INNER JOIN doctors ON doctors . hospital_id = hospitals . id INNER JOIN specialties ON doctors . specialty_id = specialties . id GROUP BY address . state, specialties . id


        $doctors = Address::query()
            ->select(["address.state", "specialties.title", DB::raw('COUNT(specialties.id) as count_main'),
            DB::raw("COUNT(ads.id) as count_additional_specialist")
            ])
            ->join('hospitals', "hospitals.address_id", "=", "address.id")
            ->join('doctors', "doctors.hospital_id", "=", "hospitals.id")
            ->join('specialties', "doctors.specialty_id", "=", "specialties.id")
            ->leftJoin('specialties as ads',function ($join){
                $join->where("doctors.specialties", "like",  DB::raw("concat('%\"', ads.id, '\"%')  "));
            } )
            ->groupBy("address.state")
            ->groupBy("specialties.id")
            ->paginate(8);

        return view('pages.statistic-doctor', compact('doctors'));
    }

    /**
     * Statistic Diagnose
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statisticDiagnose()
    {

        $diagnoses = Address::query()
            ->select(["address.state", "specialties.title",
                DB::raw("SUBSTRING_INDEX(SUBSTRING_INDEX(form_data.data, '\"diagnosis\":\"',-1), '\",\"',1) as diagnos"),
                DB::raw("count(form_data.id)")
                ])
            ->join('hospitals', "hospitals.address_id", "=", "address.id")
            ->join('doctors', "doctors.hospital_id", "=", "hospitals.id")
            ->join('specialties', "doctors.specialty_id", "=", "specialties.id")
            ->leftJoin('form_data',function ($join){
                $join->on("doctors.id", "=", "form_data.doctor_id")
                    ->where("form_data.form_id", "=", 10)
                    ->where("form_data.data", "like", '%"diagnosis":"%');
            } )

            ->groupBy("address.state")
            ->groupBy("specialties.title")
            ->groupBy("diagnos")
            ->get()->toArray();

        return view('pages.statistic-diagnose', compact('diagnoses'));
    }
}